package com.example.myapplication

import android.os.Bundle
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import androidx.appcompat.widget.PopupMenu
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView

class CrashBottomSheet:  BottomSheetDialogFragment(){

    private lateinit var buttonPopup: ImageView
    private lateinit var buttonNoPopup: ImageView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return layoutInflater.inflate(R.layout.dialog, container)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        buttonNoPopup = view.findViewById(R.id.buttonNoPopup)
        buttonPopup = view.findViewById(R.id.buttonPopup)
        buttonNoPopup.setOnClickListener {
            dismiss()
        }

        buttonPopup.setOnClickListener {
            val popup = PopupMenu(requireActivity(), buttonPopup, Gravity.END)
            popup.menuInflater.inflate(R.menu.subtitle_track_menu, popup.menu)
            popup.show()
            popup.setOnMenuItemClickListener {

                dismiss()
                true
            }
        }
    }

    override fun onResume() {
        super.onResume()
        buttonPopup.requestFocus()
    }

}